class CreateMetrics < ActiveRecord::Migration
  def change
    create_table :metrics do |t|
      t.string :metricname
      t.integer :metric_id
      #t.string :deptname
      #t.integer :dept_id

      t.timestamps
    end
  end
end
