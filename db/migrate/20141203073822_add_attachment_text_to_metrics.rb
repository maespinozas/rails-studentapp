class AddAttachmentTextToMetrics < ActiveRecord::Migration
  def self.up
    change_table :metrics do |t|
      t.attachment :text
    end
  end

  def self.down
    remove_attachment :metrics, :text
  end
end
