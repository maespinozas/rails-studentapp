class CreateDepts < ActiveRecord::Migration
  def change
    create_table :depts do |t|
      t.string :deptname
      t.integer :dept_id

      t.timestamps
    end
  end
end
