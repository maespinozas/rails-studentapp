class Metric < ActiveRecord::Base
  METRIC_OPTIONS = {'%TRACK' => 1, 'AV GPA' => 2, 'AV GPA PER TERM' => 3,
    'AV GPA PER COURSE' => 4, 'STUDENT ID, GPA' =>5,
    'COURSES,GPA, NUMBER OF STUDENTS'=>6,'All'=>7}
    has_attached_file :text,
    :url =>"/assets/scripts/:id/:style/:basename.:extension",
    :path =>":rails_root/app/assets/scripts/fakedataCustom.json"
    #:path =>":rails_root/app/assets/scripts/custom.:extension"
    #:path =>":rails_root/app/assets/scripts/:basename.:extension"
    #validates_attachment_content_type :text, :content_type => ["image/jpg", "image/jpeg", "image/gif", "image/png"]
    validates_attachment_file_name :text, :matches => [/txt\z/, /csv\z/, /json\z/]
end
