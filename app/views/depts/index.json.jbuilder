json.array!(@depts) do |dept|
  json.extract! dept, :id, :deptname, :dept_id
  json.url dept_url(dept, format: :json)
end
