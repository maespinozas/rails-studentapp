#!/usr/bin/env ruby
=begin
STUDENT ANALYTICS MAPPER

metric id metric
1       %track
2       Av GPA overall
3       Av GPA per term
4       Av GPA per Course
5       Students and GPA
6       Course GPA and number of students
=end

$metricid=0
$n_st=0
$n_st_ontrack=0
$pontrack=0

def metric(mid,line)
  case mid
  when 1  ## for each student print: ontrack,pontrack , ex. T,0.35
    is_on_track=line[line.index('track:')+7]#get boolean track get T or F form True and False, track:"True"
    ptrack=line[line.index('ptrack:')+7...line.size()-2]   #ex. ptrack:0.560254942972}
    print(is_on_track+","+ptrack+"\n")
  else
    puts("hey cindy")
    ##something
  end
end

## MAIN ###
id=1
STDIN.each_line do |line|
  metric(id,line)
end
