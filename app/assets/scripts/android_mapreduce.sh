#!/bin/bash
#print variable on a screen
#example sh ./mapreduce 1 ECE
METRIC=$1
DEPT=$2
DESTFOLDER=~
outdir=/out
mapper=~/mapperm$METRIC.rb
reducer=~/reducerm$METRIC.rb
inputdata=/input/fakedata$DEPT.json

echo "Mapper:$mapper"
echo "Reducer:$reducer"
echo "DestFolder:$DESTFOLDER"
echo "Input data:$inputdata"

if [ ! -f $DESTFOLDER/out.txt ]; then
    echo "File out.txt not found!"
else
    rm $DESTFOLDER/out.txt
    echo "deleted file out.txt"
fi


#delete /out directory
#/usr/local/hadoop/bin/hadoop fs -rm -R /out* && \
#run job
/usr/local/hadoop/bin/hadoop \
jar /usr/local/hadoop/share/hadoop/tools/lib/hadoop-streaming-2.2.0.jar \
-files $mapper,$reducer \
-mapper $mapper \
-reducer $reducer \
-input $inputdata \
-output $outdir \
&& /usr/local/hadoop/bin/hadoop fs -get $outdir/part* $DESTFOLDER/out.txt \
&& /usr/local/hadoop/bin/hadoop fs -rm -R $outdir* \
&& cat $DESTFOLDER/out.txt | sort -k 1 >> $DESTFOLDER/outtempt.txt \
&& mv $DESTFOLDER/outtempt.txt $DESTFOLDER/out.txt \
&& cat $DESTFOLDER/out.txt
