#!/bin/bash

#print variable on a screen
#FOR FOLDERS WITH SPACES QUOTE DECLARATION AND USAGE
#DEPT_OPTIONS = {'ECE' => 1, 'CS' => 2, 'EE' => 3}
METRIC=$1
DEPT=$2
DESTFOLDER="$3"
SCRIPTSFOLDER="$4"

#DESTFOLDER=${DESTFOLDER/' '/'\ '}
#SCRIPTSFOLDER=${SCRIPTSFOLDER/' '/'\ '}

mapper=$SCRIPTSFOLDER/mapperm$METRIC.rb
reducer=$SCRIPTSFOLDER/reducerm$METRIC.rb


echo "####################"
echo "Mapper:$mapper"
echo "Reducer:$reducer"
echo "DestFolder:$DESTFOLDER"
echo "####################"
if [ ! -f "$DESTFOLDER"/out.txt ]; then
    echo "File out.txt not found!"
else
    rm "$DESTFOLDER"/out.txt
    echo "deleted file out.txt"
fi

#chek if user wants all metrics
if [ "$METRIC" = '7' ];
  then
  for i in {1..6}
  do
    echo "#### Calculating metric $i ####" >> "$DESTFOLDER"/out.txt
    mapper=$SCRIPTSFOLDER/mapperm$i.rb
    reducer=$SCRIPTSFOLDER/reducerm$i.rb
    #cat "$SCRIPTSFOLDER"/fakedata.json | ruby "$mapper" | sort -k 1 | ruby "$reducer"
    cat "$SCRIPTSFOLDER"/fakedata$DEPT.json | ruby "$mapper" | sort -k 1 | ruby "$reducer"| \
    sort -k1 >> "$DESTFOLDER"/out.txt \
    && cat "$DESTFOLDER"/out.txt
  done
else
  #cat "$SCRIPTSFOLDER"/fakedata.json | ruby "$mapper" | sort -k 1 | ruby "$reducer"
  cat "$SCRIPTSFOLDER"/fakedata$DEPT.json | ruby "$mapper" | sort -k 1 | ruby "$reducer"| \
  sort -k1 >> "$DESTFOLDER"/out.txt \
  && cat "$DESTFOLDER"/out.txt
fi
